package com.project.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/persona")
public class personaRestController {

    @Autowired
    private personaRepository personaRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<persona> findAll() {
        return personaRepository.findAll();
    }
    
	@RequestMapping(method = RequestMethod.GET, value = "/{personaId}")
    public persona findOne(@PathVariable Long personaId) {
        return personaRepository.findOne(personaId);
    }
    
	@RequestMapping(method = RequestMethod.POST)
    public persona add(@RequestBody persona persona) {
        return personaRepository.save(persona);
    }

	@RequestMapping(method = RequestMethod.PUT)
    public persona update(@RequestBody persona persona) {
        return personaRepository.save(persona);
    }
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{personaId}")
    public void delete(@PathVariable Long personaId) {
        personaRepository.delete(personaId);
    }
	
}

